/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ayuda;

/**
 *
 * @author HUMBERTO VARGAS
 */
public class Ayuda {
    /*
    Hacer un count con JdbcTemplate 
    
    String query = "SELECT COUNT(*) FROM usuarios";
    int size = this.template.queryForObject(query, new Object [] {var}, Integer.class);
    
    Para un solo dato con this.template.queryForObject(query, new Object[] {var}, TipoDeDato.class);
    
    Para extraer todos los datos de una fila o extraer más de un dato con queryForList()
    
    Ej:
    String query = "select * from usuarios where id=?";
    datos = this.template.queryForList(query, "1");
    List datos = this.template.queryForList(query, param);
    
    
    3. Query for a Single Value
    It’s same like query a single row from database, uses jdbcTemplate.queryForObject()

    3.1 Single column name

	public String findCustomerNameById(Long id) {

        String sql = "SELECT NAME FROM CUSTOMER WHERE ID = ?";

        return jdbcTemplate.queryForObject(
                sql, new Object[]{id}, String.class);

    }
    Copy
    3.2 Count

    public int count() {

        String sql = "SELECT COUNT(*) FROM CUSTOMER";

        // queryForInt() is Deprecated
        // https://www.mkyong.com/spring/jdbctemplate-queryforint-is-deprecated/
        //int total = jdbcTemplate.queryForInt(sql);

        return jdbcTemplate.queryForObject(sql, Integer.class);

    }
    
    
    // Controlador
    
    En el button de x cosa que se esté haciendo, para hacer el submit
    button type="submit" le mete value="nombre"
    
    En el controlador pone
    
    @RequestMapping("nombreDeLaPagina.htm")  cambia jsp por htm acá
    public ModelAndView nombre(){
        // acciones
    }
    
    El @RequestMapping depende de lo que vaya hacer
    * Mostrar
    *RequestMethod.GET
    *RequestMethod.POST
    
    Lo que no entienda me escribe relajado
     */
}
