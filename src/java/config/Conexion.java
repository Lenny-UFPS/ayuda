/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 *
 * @author HUMBERTO VARGAS
 */
public class Conexion {

    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource data = new DriverManagerDataSource();
        data.setDriverClassName("com.mysql.jdbc.Driver");
        data.setUrl("jdbc:mysql://localhost:3306/dbname"); // dbname = nombre de la base de datos
        data.setUsername("root"); // root by default
        data.setPassword(""); // clave de root en la bd 
        return data;
    }
}
