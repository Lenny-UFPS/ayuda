/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import config.Conexion;
import entidad.Usuario;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;

/**
 *
 * @author HUMBERTO VARGAS
 */

@Controller
public class Controlador {
    
    // No corra el proyecto que no funciona, es mera información
    
    List datos;
    ModelAndView model = new ModelAndView();
    Conexion connection = new Conexion();
    JdbcTemplate template = new JdbcTemplate(connection.dataSource());
    
    
    // Mostrar el index.jsp
    @RequestMapping("index.htm")
    public ModelAndView iniciar(){
        model.addObject(new Object()); // Se usa para mostrar datos guardados en el método y mostrarlos en la plantilla HTML
        model.setViewName("index"); // Acá se pone el nombre del jsp. Ej: index.jsp  (ventana principal)
        return model;
    }
    
    // Ejemplo de como mandar y recibir los datos de la plantilla
    
    // Recibir los valores   (C R E O)
    @RequestMapping(value = "index.htm", method = RequestMethod.GET)
    public ModelAndView Login(){
        model.addObject(new Usuario()); // Como está insertando un nuevo Usuario, el get le rellena los datos automáticamente, así que en el POST ya tendrá el param con valores ya en el setter
        model.setViewName("login");
        return model;
    }
    
    // Enviar valores
    @RequestMapping(value = "index.htm", method = RequestMethod.POST)
    public ModelAndView Login(Usuario user){
        String query = "select * from usuarios where user=? and pass=?";
        datos = this.template.queryForList(query, user.getUser(), user.getPass());
        model.addObject("lista", datos); // Con esto se pueden trabajar los datos en la plantilla y mostrar
        // <c:forEach var="dato" items="lista">   dato.user --> extrae el user de la query, puede ser uno o muchos
        // con var hace referencia a el dato temporal de lista, se llama lista porque es el nombre que se le asignó a la hora de hacer el .addObject()
        return model;
    }
}
